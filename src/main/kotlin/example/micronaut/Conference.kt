package example.micronaut


data class Conference(var id: Long? = null, var name: String)