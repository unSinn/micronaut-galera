package example.micronaut

import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.withHandleUnchecked
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper
import org.jdbi.v3.sqlobject.customizer.Bind
import org.jdbi.v3.sqlobject.customizer.BindBean
import org.jdbi.v3.sqlobject.kotlin.onDemand
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys
import org.jdbi.v3.sqlobject.statement.SqlQuery
import org.jdbi.v3.sqlobject.statement.SqlUpdate
import java.util.*
import javax.inject.Singleton
import kotlin.random.Random.Default.nextLong


interface ConferenceRepository {
    @SqlUpdate("insert into conference (name) values(:name)")
    @GetGeneratedKeys
    fun save(@BindBean c: Conference): Long

    @SqlQuery("select * from conference")
    @RegisterBeanMapper(Conference::class)
    fun findAll(): List<Conference>

    @SqlQuery("select * from conference where id=:id")
    fun findById(@Bind("id") id: Long): Conference?

    @SqlUpdate("delete from conference where id=:id")
    fun remove(@Bind("id") id: Long)
}

@Singleton
class ConferenceService {

    private val db: Jdbi = DbConfig.getInstance()
    private val CONFERENCES = listOf(
            Conference(null, "Greach"),
            Conference(null, "GR8Conf EU"),
            Conference(null, "Micronaut Summit"),
            Conference(null, "Devoxx Belgium"),
            Conference(null, "Oracle Code One"),
            Conference(null, "CommitConf"),
            Conference(null, "Codemotion Madrid")
    )

    init {

        db.withHandleUnchecked { handle ->
            handle.execute("create table IF NOT EXISTS conference " +
                    "  (id INT NOT NULL AUTO_INCREMENT, name varchar(50) NOT NULL, primary key (id));")
        }

        CONFERENCES.forEach {
            save(it)
        }
    }

    fun randomConf(): Conference {
        return findById(nextLong(0L, 7L))?.let { return it } ?: return Conference(null, "nope")
    }


    fun save(c: Conference): Long? {
        return db.onDemand<ConferenceRepository>().save(c)
    }

    fun findAll(): List<Conference> {
        return db.onDemand<ConferenceRepository>().findAll()
    }

    fun findById(id: Long): Conference? {
        return db.onDemand<ConferenceRepository>().findById(id)
    }

    fun delete(id: Long): Boolean {
        return try {
            db.onDemand<ConferenceRepository>().remove(id)
            true
        } catch (e: Exception) {
            false
        }
    }
}

