package example.micronaut

import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Controller


@Controller("/conferences")
class ConferenceController(private val conferenceService: ConferenceService) {

    @Get("/random")
    fun randomConf(): Conference {
        val conf = conferenceService.randomConf()
        return if (conf.name.isNotEmpty()) {
            conf
        } else {
            Conference(null, "wooo")
        }

    }

    @Get("/test")
    fun test(): String {
        return "woot"
    }
}